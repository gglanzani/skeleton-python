try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My project',
    'author': 'Giovanni Lanzani',
    'url': 'www.lanzani.nl',
    'download_url': 'Where to download it.',
    'author_email': 'gio@lanzani.nl',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['NAME'],
    'scripts': [],
    'name': 'projectname'
}

setup(**config)
